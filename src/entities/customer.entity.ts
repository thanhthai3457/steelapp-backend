import { Entity, ObjectIdColumn, Column } from 'typeorm'
import { IsString, IsNumber, IsBoolean } from 'class-validator'
import { User } from './interface.entity'

@Entity({ name: 'customers' })
export class Customer {
  @ObjectIdColumn()
  @IsString()
  _id: string

  @Column()
  @IsString()
  code: string

  @Column()
  @IsString()
  name: string

  @Column()
  @IsString()
  mobile: string

  @Column()
  @IsString()
  address: string

  @Column()
  @IsString()
  taxCode: string

  @Column()
  @IsString()
  email: string

  @Column()
  isActive: boolean

  @Column()
  createdAt: number

  @Column()
  createdBy: User

  @Column()
  updatedAt: number

  @Column()
  updatedBy: User

  constructor(customer: any) {
    if (customer) {
      Object.assign(this, customer)
    }
  }
}