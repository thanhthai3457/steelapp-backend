import { Resolver, Query, Args, Mutation, Context } from '@nestjs/graphql'
import { ApolloError } from 'apollo-server-express'
import { getMongoRepository } from 'typeorm'
import {
  StoreShop as StoreShopType
} from 'graphql.schema'
import { StoreShop, UserEntity } from 'entities/entity.index'
import { v4 as uuidv4 } from 'uuid'
import moment from 'moment'

@Resolver('StoreShop')
export class StoreShopResolver {
  @Query('getStores')
  async getStores (): Promise<StoreShopType[] | ApolloError> {
    try {
      const stores = await getMongoRepository(StoreShop).find({
        where: {
          isActive: true,
          isShop: true
        }
      })
      return stores.sort((a, b) => a.name > b.name ? 1 : -1)
    } catch (error) {
      return new ApolloError(error)
    }
  }

  @Mutation('createStoreShop')
  async createStoreShop (
    @Args() args,
    @Context() { currentUser }
  ): Promise<String | ApolloError> {
    try {
      const { info } = args
      const existed = await getMongoRepository(StoreShop).findOne({
        where: {
          isActive: true,
          code: info.code
        }
      })
      if (existed)
        return new ApolloError('This code already exist!', '400')
      const init = {
        _id: uuidv4(),
        ...info,
        isActive: true,
        createdAt: moment().valueOf(),
        createdBy: {
          _id: currentUser._id,
          username: currentUser.username,
          fullname: `${currentUser.firstname} ${currentUser.lastname}`
        }
      }
      const create = await getMongoRepository(StoreShop).insertOne(init)
      if (create.insertedCount) return 'success'
      return 'failed'
    } catch (error) {
      console.log(error)
      return new ApolloError(error)
    }
  }

  @Mutation('updateStoreShop')
  async updateStoreShop (
    @Args() args,
    @Context() { currentUser }
  ): Promise<String | ApolloError> {
    try {
      const { id, info } = args
      const existed = await getMongoRepository(StoreShop).findOne({
        where: {
          isActive: true,
          code: info.code
        }
      })
      if (existed && existed._id !== id)
        return new ApolloError('This code already exist!', '400')
      const init = {
        ...info,
        isActive: true,
        createdAt: moment().valueOf(),
        createdBy: {
          _id: currentUser._id,
          username: currentUser.username,
          fullname: `${currentUser.firstname} ${currentUser.lastname}`
        }
      }
      const updateS = await getMongoRepository(StoreShop).updateOne(
        {
          _id: id
        },
        {
          $set: {
            ...init
          }
        }
      )
      if (updateS.modifiedCount) return 'success'
      return 'failed'
    } catch (error) {
      console.log(error)
      return new ApolloError(error)
    }
  }

  @Mutation('removeStoreShop')
  async removeStoreShop (
    @Args('ids') ids,
    @Context() { currentUser }
  ): Promise<string | ApolloError> {
    try {
      const user = await getMongoRepository(UserEntity).findOne({
        _id: currentUser._id
      })
      const data = await getMongoRepository(StoreShop).updateMany(
        {
          _id: {
            $in: ids
          }
        },
        {
          $set: {
            isActive: false,
            updatedAt: moment().valueOf(),
            updatedBy: {
              _id: user._id,
              username: user.username,
              fullname: `${user.firstname} ${user.lastname}`
            }
          }
        }
      )
      if (data.modifiedCount > 0) return 'success'
      return new ApolloError('Store or shop does not exist', '400')
    } catch (error) {
      return new ApolloError(error, '500')
    }
  }
}