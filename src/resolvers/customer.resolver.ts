import { Resolver, Query, Args, Mutation, Context } from '@nestjs/graphql'
import { ApolloError } from 'apollo-server-express'
import { getMongoRepository } from 'typeorm'
import {
  Customer as CustomerType
} from 'graphql.schema'
import { Customer, UserEntity } from 'entities/entity.index'
import moment from 'moment'

@Resolver('Customer')
export class CustomerResolver {
  @Query('getCustomers')
  async getCustomers (): Promise<CustomerType[] | ApolloError> {
    try {
      const cus = await getMongoRepository(Customer).find({
        where: {
          isActive: true
        }
      })
      return cus.sort((a, b) => a.createdAt > b.createdAt ? -1 : 1)
    } catch (error) {
      return new ApolloError(error)
    }
  }

  @Mutation('removeCustomer')
  async removeCustomer (
    @Args('ids') ids,
    @Context() { currentUser }
  ): Promise<string | ApolloError> {
    try {
      const user = await getMongoRepository(UserEntity).findOne({
        _id: currentUser._id
      })
      const data = await getMongoRepository(Customer).updateMany(
        {
          _id: {
            $in: ids
          }
        },
        {
          $set: {
            isActive: false,
            updatedAt: moment().valueOf(),
            updatedBy: {
              _id: user._id,
              username: user.username,
              fullname: `${user.firstname} ${user.lastname}`
            }
          }
        }
      )
      if (data.modifiedCount > 0) return 'success'
      return new ApolloError('This customer does not exist', '400')
    } catch (error) {
      return new ApolloError(error, '500')
    }
  }
}